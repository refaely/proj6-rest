<html>
    <head>
        <title>API</title>
    </head>

    <body>
        <h3>/listAll</h3>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');

            $obj = json_decode($json);
            $times = $obj->result;
                
            if ($times == []){
                echo "The database is empty";
            } else {
                foreach($times as $k) {
                    $distance = $k->km;
                    $open = $k->open;
                    $close = $k->close;
                    echo "<li><b>Distance: $distance</b></li><li>Open: $open</li><li>Close: $close</li>";
                }
            }
            ?>
        </ul>
        <h3>/listOpenOnly</h3>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly');

            $obj = json_decode($json);
            $times = $obj->result;
                
            if ($times == []){
                echo "The database is empty";
            } else{
                foreach($times as $k) {
                    $distance = $k->km;
                    $open = $k->open;
                    echo "<li><b>Distance: $distance</b></li><li>Open: $open</li>";
                }
            }
            ?>
        </ul>
        <h3>/listCloseOnly</h3>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly');

            $obj = json_decode($json);
            $times = $obj->result;
                
            if ($times == []){
                echo "The database is empty";
            } else{
                foreach($times as $k) {
                    $distance = $k->km;
                    $close = $k->close;
                    echo "<li><b>Distance: $distance</b></li><li>Close: $close</li>";
                }
            }
            ?>
        </ul>

         <h3>/listAll/csv</h3>
        <ul>
        <?php
        $entry = file_get_contents('http://laptop-service/listAll/csv');
        echo nl2br($entry);
                   
            ?>
        </ul>

        <h3>/listOpenOnly/csv</h3>
        <ul>
            <?php
            $data = file_get_contents('http://laptop-service/listOpenOnly/csv');
            echo nl2br($data);
            ?>
        </ul>

           <h3>/listCloseOnly/csv</h3>
        <ul>
            <?php
            $data = file_get_contents('http://laptop-service/listCloseOnly/csv');
            echo nl2br($data);
            ?>
        </ul>
    </body>
    </html>
  




       
