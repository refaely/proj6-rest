Author: Refael Yehuda, refaely@uoregon.edu

Class: CIS 322

Date: 11/20/18

# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

## Directions:
* Run 'docker-compose up --build' in the 'DockerRestAPI' directory.
* In your browser:
    * Go to <localhost:5000> to open the ACP Brevet Times Calculator.
    * Go to <localhost:5001> to acess the RESTful service.
    * Go to <localhost:5002>/<API> to access a specific API service such as '/listAll', '/listOpenOnly', or '/listCloseOnly' (You can acess all the services that are in the prompt of the assignment).
        * Add '?top=NUM' to the URL with any NUM to display a set amount of the results.
        * For example:   <localhost:5002>/listOpenOnly/json?top=5
            
            
            








